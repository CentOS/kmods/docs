Packages provided by the Kmods SIG are separated into different repositories.
Release packages are provided to easily enable these repositories.
See [Quickstart](install.md) on how to gain access to these release packages.

# Kernel modules
The Kmods SIG provides kernel modules built for the stock Enterprise Linux kernel.
To gain access to these you may install `centos-release-kmods`:

```shell
$ sudo dnf install centos-release-kmods
```

Once installed you have access to all packages in `packages-main` and `packages-userspace`.
`packages-main` contains kernel modules which are extensions to the stock kernel with their related user space packages in `packages-userspace`.

In addition the Kmods SIG provides kernel modules modifying those part of the stock kernel.
In particular the source code for these kernel modules has been modified to restore support for devices that have been explicitly removed.
These are available from `packages-rebuild` which can be enabled once `centos-release-kmods` is installed by:
```shell
$ sudo dnf config-manager --set-enabled centos-kmods-rebuild
```

# Kernels
The Kmods SIG provides different Linux kernel versions with a Fedora flavored configuration, i.e., with more modules enabled.

## LTS
The Kmods SIG intends to provide all LTS kernels for a specific major Enterprise Linux release released during the first 5 years of full support.
This means that typically 5 LTS kernels are provided for each Enterprise Linux.
During the maintenance support phase, updates to LTS kernels are provided, but not new kernel versions.

To gain access to these you may install `centos-release-kmods-kernel-<version>`, where you replace `<version>` by the kernel version you want to install, e.g.:

```shell
$ sudo dnf install centos-release-kmods-kernel-6.6
```

The newer kernel will then be automatically installed when you update your system.
See [packages](packages.md) for a list of available LTS kernels for each Enterprise Linux.

## Latest
In addition to the LTS kernel version, the Kmods SIG provides the latest stable kernel.
Until the last LTS kernel for a particular version is released, the latest kernel is provided for that Enterprise Linux.

To gain access to the latest stable kernel you may install `centos-release-kmods-kernel`:

```shell
$ sudo dnf install centos-release-kmods-kernel
```

The newer kernel will then be automatically installed when you update your system.
