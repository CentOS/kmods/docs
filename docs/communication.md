There are various ways to communicate with the Kmods SIG.

# Report Bugs/Issues/RFE
Please report any issue specific to a certain package in the corresponding project's issue tracker on [GitLab/CentOS/kmods](https://gitlab.com/CentOS/kmods).
A project for each package is available in [rpms](https://gitlab.com/CentOS/kmods/rpms).
For some packages an additional project exist in [src](https://gitlab.com/CentOS/kmods/src).
In case two projects exist for a certain package, please use the one in [src](https://gitlab.com/CentOS/kmods/src) to submit issues.

In case the issue is not related to a specific package or you want to request a new package please use the [Kmods SIG Issue Tracker](https://gitlab.com/CentOS/kmods/sig/-/issues).

# IRC
For interactive communication or short question IRC is best suited.
You can find us in #centos-kmods on [Libera.Chat](https://libera.chat/).

# Mailing list
Mailing list is mainly used for coordination and discuss larger issues.
We do not have a Kmods SIG specific mailing list, hence we use [devel@lists.centos.org](https://lists.centos.org/postorius/lists/devel.lists.centos.org/) for this purpose.
