Everybody is welcome to join and contribute to the Kmods SIG.
Membership can be applied for via any of the listed means of [communication](communication.md).
Any current member can raise objections and request a simple majority vote on membership applications.
SIG members are expected to actively contribute or otherwise remain engaged with the project.
Stale members may be removed by a simple majority vote after six months of inactivity.
