This page collects meeting minutes for the Kmods SIG.

# 2024
* [2024-04-01](https://www.centos.org/minutes/2024/April/centos-meeting.2024-04-01-16.00.html)
* [2024-02-05](https://www.centos.org/minutes/2024/February/centos-meeting.2024-02-05-16.00.html)

# 2023
* [2023-12-04](https://www.centos.org/minutes/2023/December/centos-meeting.2023-12-04-16.00.html)
* [2023-11-06](https://www.centos.org/minutes/2023/November/centos-meeting.2023-11-06-15.00.html)
* [2023-10-02](https://www.centos.org/minutes/2023/October/centos-meeting.2023-10-02-15.00.html)
* [2023-09-04](https://www.centos.org/minutes/2023/September/centos-meeting.2023-09-04-15.01.html)
* [2023-08-07](https://www.centos.org/minutes/2023/August/centos-meeting.2023-08-07-15.00.html)
* [2023-07-03](https://www.centos.org/minutes/2023/July/centos-meeting.2023-07-03-15.01.html)
* [2023-04-03](https://www.centos.org/minutes/2023/April/centos-meeting.2023-04-03-16.00.html)
* [2023-03-06](https://www.centos.org/minutes/2023/March/centos-meeting.2023-03-06-16.00.html)
* [2023-02-06](https://www.centos.org/minutes/2023/February/centos-meeting.2023-02-06-16.00.html)
* [2023-01-02](https://www.centos.org/minutes/2023/January/centos-meeting.2023-01-02-16.00.html)

# 2022
* [2022-12-05](https://www.centos.org/minutes/2022/December/centos-meeting.2022-12-05-16.00.html)
* [2022-11-07](https://www.centos.org/minutes/2022/November/centos-meeting.2022-11-07-16.00.html)
* [2022-09-05](https://www.centos.org/minutes/2022/September/centos-meeting.2022-09-05-16.00.html)
* [2022-08-01](https://www.centos.org/minutes/2022/August/centos-meeting.2022-08-01-15.00.html)
* [2022-07-04](https://centbot.centos.org/minutes/2022/July/centos-meeting.2022-07-04-15.00.html)
* [2022-06-06](https://centbot.centos.org/minutes/2022/June/centos-meeting.2022-06-06-15.00.html)
* [2022-05-02](https://www.centos.org/minutes/2022/May/centos-meeting.2022-05-02-15.00.html)
* [2022-03-07](https://www.centos.org/minutes/2022/March/centos-meeting.2022-03-07-16.00.html)
* [2022-02-21](https://www.centos.org/minutes/2022/February/centos-meeting.2022-02-21-16.00.html)
* [2022-02-07](https://www.centos.org/minutes/2022/February/centos-meeting.2022-02-07-16.00.html)
* [2022-01-24](https://www.centos.org/minutes/2022/January/centos-meeting.2022-01-24-16.00.html)
* [2022-01-10](https://www.centos.org/minutes/2022/January/centos-meeting.2022-01-10-16.00.html)

# 2021
* [2021-12-13](https://www.centos.org/minutes/2021/December/centos-meeting.2021-12-13-16.00.html)
* [2021-11-29](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-29-15.02.html)
* [2021-11-15](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-15-15.00.html)
* [2021-11-01](https://www.centos.org/minutes/2021/November/centos-meeting.2021-11-01-15.00.html)
* [2021-10-18](https://www.centos.org/minutes/2021/October/centos-meeting.2021-10-18-15.00.html)
* [2021-10-04](https://www.centos.org/minutes/2021/October/centos-meeting.2021-10-04-15.00.html)
* [2021-09-20](https://www.centos.org/minutes/2021/September/centos-meeting.2021-09-20-15.00.html)
* [2021-09-06](https://www.centos.org/minutes/2021/September/centos-meeting.2021-09-06-15.04.html)
* [2021-08-23](https://www.centos.org/minutes/2021/August/centos-meeting.2021-08-23-15.00.html)
* [2021-08-09](https://www.centos.org/minutes/2021/August/centos-meeting.2021-08-09-15.00.html)
* [2021-07-26](https://www.centos.org/minutes/2021/July/centos-meeting.2021-07-26-15.04.html)
* [2021-07-12](https://www.centos.org/minutes/2021/July/centos-meeting.2021-07-12-15.00.html)
* [2021-06-28](https://www.centos.org/minutes/2021/June/centos-meeting.2021-06-28-15.11.html)
