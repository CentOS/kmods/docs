The SIG publishes quarterly reports on the [CentOS Blog](https://blog.centos.org).

# 2024
* [2024H2](https://blog.centos.org/2025/02/february-2025-news/)
* [2024H1](https://blog.centos.org/2024/06/june-2024-news/)

# 2023
* [2023Q4](https://blog.centos.org/2024/01/january-2024-news/)
* [2023Q2](https://blog.centos.org/2023/09/centos-newsletter-september-2023/)
* [2023Q1](https://blog.centos.org/2023/05/centos-newsletter-may-2023/)

# 2022
* [2022Q4](https://blog.centos.org/2023/01/centos-community-newsletter-january-2023/)
* [2022Q3](https://blog.centos.org/2022/10/centos-community-newsletter-october-2022/)
* [2022Q2](https://blog.centos.org/2022/07/centos-community-newsletter-july-2022/)
* [2022Q1](https://blog.centos.org/2022/04/centos-community-newsletter-april-2022/)

# 2021
* [2021Q4](https://blog.centos.org/2022/01/centos-community-newsletter-january-2022/)
* [2021Q3](https://blog.centos.org/2021/10/kmods-sig-quarterly-report-2/)
* [2021Q2](https://blog.centos.org/2021/07/kmods-sig-quarterly-report/)
