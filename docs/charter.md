# Goals
The kmods SIG focuses on providing kernel modules currently not available in CentOS Stream.

# Status
**Approved**

Sponsoring Governing Board Member: Patrick Riehecky

# What's in scope
This SIG is a good place for any kernel module that is beneficial to CentOS Stream, but cannot be directly contributed to any of the involved upstream projects.
These kernel modules may be divided in three categories:

## Restore support for deprecated devices
The CentOS Stream kernel includes several kernel modules for which the list of supported devices has been limited by Red Hat.
This SIG aims to provide versions of these kernel modules with restored support for as many deprecated and removed devices as possible.

## In-tree kernel modules not enabled for CentOS Stream
Many in-tree kernel modules are simply disabled for the CentOS Stream kernel. This may either be due to drivers being deprecated and removed compared to older CentOS major releases or never being enabled in the first place.
This SIG aims to provide these in-tree drivers as external kernel modules to enable CentOS Stream running on a broader range of available hardware and provide other beneficial functionality.

## Out-of-tree kernel modules
This SIG also aims to provide out-of-tree kernel modules for CentOS Stream.
Due to legal reasons, out-of-tree kernel modules with a non GPL v2 compatible license require a case by case review by Red Hat Legal.

## User space tools
User space tools required by or specific to kernel modules provided by this SIG, which are not suitable to be included in EPEL, shall be provided by this SIG as well.

# What's not in scope
Anything that can be contributed directly to any of the involved upstream projects is not in scope.
This includes, but is not limited to:

* Unrelated user space packages: These should be submitted to Fedora/EPEL.
* Support for new architectures currently not supported by CentOS Stream.
* Kernel modules with a non GPL v2 compatible license and no exception given by Red Hat Legal.

# Collaboration
It is desired to work closely together with other groups working on similar tasks within the CentOS Stream community and the broader Enterprise Linux community.
In particular this includes, but is not limited to:

## kernel-plus
Especially for in-tree kernel modules it is desired to closely work together with the kernel-plus developers/maintainers.

## CentOS Stream kernel
A close collaboration with upstream is desired to get any valuable kernel module directly into the CentOS Stream kernel.

## ELRepo
There is a large overlap with ELRepo for many of the kernel modules to be provided by this SIG for CentOS Stream.
Hence we hope to establish a good connection with the ELRepo community which shall be beneficial to both sides.

# Roadmap

* Provide packages for in-tree kernel modules with restored support for deprecated devices.
* Provide packages for in-tree kernel modules that have been supported in older CentOS major releases.
* Provide packages for further in- and out-of-tree kernel modules as requested by the community.
* Work with other SIGs and others involved to establish a common work-flow to sign kernels and/or kernel modules provided by SIGs.
