We offer release packages containing our repository configuration files and [public package signing key](https://www.centos.org/keys/RPM-GPG-KEY-CentOS-SIG-Kmods).

On some Enterprise Linux distributions, in particular on CentOS Stream, these release packages are available from the default repositories.
On other distributions it is required to first add a repository by:

!!! warning
    Do not follow these steps on CentOS Stream instead directly move on to the list of [repositories](repositories.md).

1. Import our [public package signing key](https://www.centos.org/keys/RPM-GPG-KEY-CentOS-SIG-Kmods):

    ```
    $ sudo rpm --import https://www.centos.org/keys/RPM-GPG-KEY-CentOS-SIG-Kmods
    ```

1. Install package containing main repository configuration:

    ```
    $ sudo dnf install https://mirror.stream.centos.org/SIGs/$(rpm --eval '%{?rhel}/kmods/%{_arch}/repos-main/Packages/c/centos-repos-kmods-%{?rhel}-2.el%{?rhel}.noarch.rpm')
    ```

See [repositories](repositories.md) for a list of available repositories and their corresponding release package to be installed.
