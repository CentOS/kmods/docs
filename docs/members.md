# Current members

* Peter Georg
* Patrick Riehecky

# Chairs
The SIG is co-chaired by two equal chairpersons elected by SIG members for one year.
Each chairperson is elected individually using a plurality vote.

The SIG is currently co-chaired by Peter Georg and <to be elected\>.

The next SIG chair election is scheduled to be held in <to be determined\>.

# Former members
* Jonathan Billings
