The [Kmods SIG](charter.md) focuses on two aspects:

* Packaging and maintaining kernel modules for the stock Enterprise Linux kernel.
* Packaging and maintaining Fedora flavored kernels for Enterprise Linux distributions.

We welcome anybody that's interested and willing to do work within the scope of the Kmods SIG to join and contribute.
See [membership](membership.md) for how to join.
