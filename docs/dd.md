# Driver Discs
<table border="all">
    <tr>
        <th></th>
        <th style="text-align: center", colspan=3>packages-main</th>
        <th style="text-align: center", colspan=3>packages-rebuild</th>
    </tr>
    <tr>
        <th>Enterprise Linux 8</th>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/aarch64/packages-main/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/ppc64le/packages-main/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/x86_64/packages-main/DriverDiscs/">x86_64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/aarch64/packages-rebuild/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/ppc64le/packages-rebuild/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/8/kmods/x86_64/packages-rebuild/DriverDiscs/">x86_64</a></td>
    </tr>
    <tr>
        <th>Stream 9</th>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/aarch64/packages-main/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/ppc64le/packages-main/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/x86_64/packages-main/DriverDiscs/">x86_64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/aarch64/packages-rebuild/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/ppc64le/packages-rebuild/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9-stream/kmods/x86_64/packages-rebuild/DriverDiscs/">x86_64</a></td>
    </tr>
    <tr>
        <th>Enterprise Linux 9</th>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/aarch64/packages-main/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/ppc64le/packages-main/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/x86_64/packages-main/DriverDiscs/">x86_64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/aarch64/packages-rebuild/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/ppc64le/packages-rebuild/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/9/kmods/x86_64/packages-rebuild/DriverDiscs/">x86_64</a></td>
    </tr>
    <tr>
        <th>Stream 10</th>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/aarch64/packages-main/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/ppc64le/packages-main/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/x86_64/packages-main/DriverDiscs/">x86_64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/aarch64/packages-rebuild/DriverDiscs/">aarch64</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/ppc64le/packages-rebuild/DriverDiscs/">ppc64le</a></td>
        <td style="text-align: center"><a href="https://mirror.stream.centos.org/SIGs/10-stream/kmods/x86_64/packages-rebuild/DriverDiscs/">x86_64</a></td>
    </tr>
</table>

# Driver Disc Creator
The Kmods SIG provides a tool called [Driver Disc Creator](https://gitlab.com/CentOS/kmods/ddc) to create driver discs including kernel module packages specified by the user.
It is currently limited to kernel module packages provided by the Kmods SIG.
Run `ddc.sh --help` for information on usage.
