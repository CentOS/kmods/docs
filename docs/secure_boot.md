When Secure Boot is enabled, the system boot loaders, the kernel, and all kernel modules have to be signed with a private key and authenticated with the corresponding public key.
All kernels and kernel modules provided by the kmods SIG are currently not signed with a private key.
Hence it is required to disable Secure Boot to be able to use any of these.
