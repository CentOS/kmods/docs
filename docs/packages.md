Please see CBS for currently available packages for each repository:

<table border="all">
    <tr>
        <th></th>
        <th style="text-align: center">Enterprise Linux 8</th>
        <th style="text-align: center">Stream 9</th>
        <th style="text-align: center">Enterprise Linux 9</th>
        <th style="text-align: center">Stream 10</th>
    </tr>
    <tr>
        <th>packages-main</th>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2350">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2402">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2636">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=3019">CBS</a></td>
    </tr>
    <tr>
        <th>packages-userspace</th>
        <td style="text-align: center" colspan="1"><a href="https://cbs.centos.org/koji/packages?tagID=2720">CBS</a></td>
        <td style="text-align: center" colspan="2"><a href="https://cbs.centos.org/koji/packages?tagID=2728">CBS</a></td>
        <td style="text-align: center" colspan="1"></td>
    </tr>
    <tr>
        <th>packages-rebuild</th>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2346">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2406">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=2640">CBS</a></td>
        <td style="text-align: center"><a href="https://cbs.centos.org/koji/packages?tagID=3023">CBS</a></td>
    </tr>
    <tr>
        <th>kernel-6.1</th>
        <td style="text-align: center" colspan="1"><a href="https://cbs.centos.org/koji/packages?tagID=2874">CBS</a></td>
        <td style="text-align: center" colspan="2"><a href="https://cbs.centos.org/koji/packages?tagID=2878">CBS</a></td>
        <td style="text-align: center" colspan="1"></td>
    </tr>
    <tr>
        <th>kernel-6.6</th>
        <td style="text-align: center" colspan="1"><a href="https://cbs.centos.org/koji/packages?tagID=2862">CBS</a></td>
        <td style="text-align: center" colspan="2"><a href="https://cbs.centos.org/koji/packages?tagID=2866">CBS</a></td>
        <td style="text-align: center" colspan="1"></td>
    </tr>
    <tr>
        <th>kernel-6.12</th>
        <td style="text-align: center" colspan="1"></td>
        <td style="text-align: center" colspan="2"><a href="https://cbs.centos.org/koji/packages?tagID=3067">CBS</a></td>
        <td style="text-align: center" colspan="1"></td>
    </tr>
    <tr>
        <th>kernel-latest</th>
        <td style="text-align: center" colspan="1"></td>
        <td style="text-align: center" colspan="2"><a href="https://cbs.centos.org/koji/packages?tagID=2870">CBS</a></td>
        <td style="text-align: center" colspan="1"></td>
    </tr>
</table>
